<?php

function u2f_admin_form($form, &$form_state) {
  $form = array();
  $config = variable_get('u2f_config', array());
  $texts  = variable_get('u2f_texts',  array());

  $form['enabled'] = array(
  '#title'         => t('Enable U2F'),
  '#type'          => 'radios',
  '#options'       => array(
    'y' => t('Enabled'   ),
    'n' => t('Disabled'  ),
    'd' => t('Debug mode'),
   ),
  '#default_value' => $config['enabled']?$config['enabled']:'n',
  '#description'   => t('Whether users should be asked to use a U2F token. Debug will only ask the roles assigned as debug role below.'),
  '#required'      => TRUE,
  );

  $form['registration'] = array(
    '#title'         => t('Enable new U2F devices to be registered'),
    '#type'          => 'radios',
    '#options'       => array(
      'y' => t('Enabled'   ),
      'n' => t('Disabled'  ),
     ),
    '#default_value' => $config['registration']?$config['registration']:'y',
    '#description'   => t('Whether users with no registered token should be allowed to register new tokens.'),
    '#required'      => TRUE,
  );

  $form['area'] = array(
    '#title'         => t('Area of effect config'),
    '#type'          => 'fieldset',
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );

  $roles = user_roles(TRUE, NULL);

  $form['area']['role_debug'] = array(
    '#title'         => t('Debug roles'),
    '#description'   => t('Select the roles which should be asked for tokens when in debug mode'),
    '#type'          => 'checkboxes',
    '#options'       => $roles,
    '#default_value' => $config['role_debug'],
    '#states'        => array(
      'visible' => array(
        ':input[name="enabled"]' => array('value' => 'd'),
      ),
    ),
  );

  $form['area']['role_whitelist'] = array(
    '#title'         => t('Whitelisted roles'),
    '#description'   => t('Select the roles which should never be required to have tokens.'),
    '#type'          => 'checkboxes',
    '#options'       => $roles,
    '#default_value' => $config['role_whitelist'],
  );

  // Path system almost entirely copied over from the block module.
  $access = user_access('use PHP for settings');
  $options = array(
    'white' => t('All pages except those listed'),
    'black' => t('Only the listed pages'),
  );
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

  if (module_exists('php') && $access) {
    $options += array('php' => t('Pages on which this PHP code returns <code>TRUE</code> (experts only)'));
    $description .= ' ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
  }

  $form['area']['visibility'] = array(
    '#type'          => 'radios',
    '#title'         => t('Show block on specific pages'),
    '#options'       => $options,
    '#default_value' => isset($config['visibility']) ? $config['visibility'] : BLOCK_VISIBILITY_NOTLISTED,
    '#required'      => TRUE,
  );
  $form['area']['pages'] = array(
    '#type'          => 'textarea',
    '#title'         => '<span class="element-invisible">Path patterns</span>',
    '#default_value' => isset($config['pages']) ? $config['pages'] : '',
    '#description'   => $description,
  );

  $form['time'] = array(
    '#title'         => t('Allowed times'),
    '#type'          => 'fieldset',
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );

  $form['time']['token'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Seconds to wait for token device'),
    '#default_value'    => isset($config['time']['token'])?$config['time']['token']:300,
    '#description'      => t('Amount of seconds to wait for token to be touched when requested'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required'         => TRUE,
  );

  $form['time']['expires'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Seconds to allow usage after token verification'),
    '#default_value'    => isset($config['time']['expires'])?$config['time']['expires']:300,
    '#description'      => t('Amount of seconds to grant access after token has been verified'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required'         => TRUE,
  );

  $form['text'] = array(
    '#title'         => t('Displayed text'),
    '#type'          => 'fieldset',
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );

  $form['text']['register'] = array(
    '#title'         => t('Device registration text'),
    '#description'   => t('The text to show when asking the user to register their device.'),
    '#type'          => 'text_format',
    '#default_value' => @$texts['register']['value'],
    '#format'        => isset($texts['register']['format'])?$texts['register']['format']:NULL,
  );

  $form['text']['noregister'] = array(
    '#title'         => t('No registration allowed'),
    '#description'   => t('The text to show when the user does not have a token and registration is closed'),
    '#type'          => 'text_format',
    '#default_value' => @$texts['noregister']['value'],
    '#format'        => isset($texts['noregister']['format'])?$texts['noregister']['format']:NULL,
  );

  $form['text']['sign'] = array(
    '#title'         => t('Device signing text'),
    '#description'   => t('The text to show when asking the user to sign a challenge with their device.'),
    '#type'          => 'text_format',
    '#default_value' => @$texts['sign']['value'],
    '#format'        => isset($texts['sign']['format'])?$texts['sign']['format']:NULL,
  );

  $form['subgmit'] = array(
    '#type'          => 'submit',
    '#value'         => 'Submit'
  );

  return $form;

}

function u2f_admin_form_validate($form, &$form_state) {
  if($form_state['values']['enabled'] == 'd' && count(array_filter($form_state['values']['role_debug']))==0) {
    form_set_error('role_debug', t('At least one debug role must be selected to enable it.'));
  }
}

function u2f_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  variable_set('u2f_config', array(
    'enabled'        => $values['enabled'],
    'registration'   => $values['registration'],
    'role_debug'     => array_filter($values['role_debug']),
    'role_whitelist' => array_filter($values['role_whitelist']),
    'visibility'     => $values['visibility'],
    'pages'          => trim($values['pages']),
    'time'           => array(
      'token'   => $values['token'],
      'expires' => $values['expires'],
    ),
  ));

  variable_set('u2f_texts', array(
    'register'   => $values['register'],
    'noregister' => $values['noregister'],
    'sign'       => $values['sign'],
  ));
}
