<?php

function u2f_switchboard() {
  global $user;

  drupal_add_js(drupal_get_path('module','u2f').'/js/u2f-api.js');
  drupal_add_js(drupal_get_path('module','u2f').'/js/u2f.js');

  $u2f = u2f_getlib();

  // Check if user has registered tokens
  $regs = u2f_get_user_registrations();
  if(!is_array($regs)) { $regs = array(); }
  $config = variable_get('u2f_config', array());
  $texts  = variable_get('u2f_texts',  array());

  if(count($regs)) {
    // User already has registered tokens
    $out  = '<div class="u2f-intro">';
    $out .= check_markup($texts['sign']['value'], $texts['sign']['format']);
    $out .= '<p>You will then be given access for <strong>'.round($config['time']['expires']/60).'</strong> minutes.</p>';
    $out .= '</div>';

    $req = $u2f->getAuthenticateData($regs);
    $req = $req[0];

    if(!isset($_SESSION['u2f']['lock']) || $_SESSION['u2f']['lock']==0) {
      $_SESSION['u2f']['sign_req'] = json_encode($req);
      $_SESSION['u2f']['lock']=1;
    }

    drupal_add_js(
      array('u2f' => array(
        'action'    => 'sign',
        'appId'     => $req->appId,
        'challenge' => $req->challenge,
        'keyHandle' => $req->keyHandle,
        'version'   => $req->version,
        'wait'      => $config['time']['token']
      )),
      'setting'
    );
    return $out;

  } else {
    // No tokens registered yet
    $out  = '<div class="u2f-intro">';
    $out .= '<p><b>You do NOT have a token registered.</b></p>';
    $out .= '<br />';

    if(U2F_ALLOW_REGISTER) {
      $out .= check_markup($texts['register']['value'], $texts['register']['format']);
      $out .= '<p>You will then be given access for <strong>'.round($config['time']['expires']/60).'</strong> minutes.</p>';

      // Ask our JS to do the registration, pass data
      $reg_challenge = $u2f->getRegisterData($regs);
      $reg_challenge = $reg_challenge[0];

      if(!isset($_SESSION['u2f']['lock']) || $_SESSION['u2f']['lock']==0) {
        $_SESSION['u2f']['register_challenge'] = $reg_challenge->challenge;
        $_SESSION['u2f']['lock']=1;
      } else {
        // Why oh why does this happen?
      }

      drupal_add_js(
        array('u2f' => array(
          'action'    => 'register',
          'version'   => $reg_challenge->version,
          'challenge' => $reg_challenge->challenge,
          'appId'     => $reg_challenge->appId,
          'wait'      => $config['time']['token'],
        )),
        'setting'
      );

    } else {
      $out .= check_markup($texts['noregister']['value'], $texts['noregister']['format']);
    }

    $out .= '</div>';
  }

  return $out;

}
