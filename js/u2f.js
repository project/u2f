(function ($) {
  Drupal.behaviors.u2f = {
    attach: function (context, settings) {
      var status = $('h1#page-title');

      // If we have nothing to do bail out
      if(typeof u2f === "undefined" || typeof Drupal.settings.u2f === "undefined") { return; }

      // Make sure we only bind things once
      $('body').once('u2f', function() {
        switch(Drupal.settings.u2f.action) {
          // If we've been asked to register a new device
          case 'register':
            // Load config from backend
            var appId = Drupal.settings.u2f.appId;
            var registerRequests = [{version: Drupal.settings.u2f.version, challenge: Drupal.settings.u2f.challenge}];

            // Call clientside library with given params
            u2f.register(appId, registerRequests, [], function(tokendata) {
              ///TODO: Check status and bail if we have an invalid token / if we have a different browser etc

              status.html('U2F Token Verification - Sending device registration to server...');
              // Send challenge response to server
              $.post('async/u2f-endpoint', { action: 'register', response: JSON.stringify(tokendata) }, function(serverdata) {
                // Though extremely unlikely we may have a failure
                if(serverdata.status == 'OK') {
                  status.html('U2F Token Verification - Registration successful!').addClass('status');
                  window.location.href = serverdata.redirect;
                } else {
                  status.html('U2F Token Verification - Registration failed!').addClass('error');
                }
              });
            });

            break;
          case 'sign':
            // Get config from backend
            var appId = Drupal.settings.u2f.appId;
            var challenge = Drupal.settings.u2f.challenge;
            var registeredKeys = [{version: Drupal.settings.u2f.version, keyHandle: Drupal.settings.u2f.keyHandle }]

            // Call clientside library with given params
            u2f.sign(appId, challenge, registeredKeys, function(tokendata) {
              ///TODO: Check status and bail if we have an invalid token
              status.html('U2F Token Verification - Sending device signature to server...');

              // Send server the challenge response
              $.post('async/u2f-endpoint', { action: 'sign', response: JSON.stringify(tokendata) }, function(serverdata) {
                  if(serverdata.status == 'OK') {
                    status.html('U2F Token Verification - Signature matches!').addClass('status');
                    window.location.href = serverdata.redirect;
                  } else {
                    status.html('U2F Token Verification - Signature differs!').addClass('error');
                  }
              });
            });
            break;
          default:
            // Do nothing
        }
      });
    }
  };

}(jQuery));